/* 
TEST Dungeon_set
*/


/* 
vendors
*/
REPLACE INTO `world-tbc`.`creature_template`(`Entry`, `Name`, `SubName`, `IconName`, `MinLevel`, `MaxLevel`, `HeroicEntry`, `ModelId1`, `ModelId2`, `ModelId3`, `ModelId4`, `Faction`, `Scale`, `Family`, `CreatureType`, `InhabitType`, `RegenerateStats`, `RacialLeader`, `NpcFlags`, `UnitFlags`, `DynamicFlags`, `ExtraFlags`, `CreatureTypeFlags`, `SpeedWalk`, `SpeedRun`, `Detection`, `CallForHelp`, `Pursuit`, `Leash`, `Timeout`, `UnitClass`, `Rank`, `Expansion`, `HealthMultiplier`, `PowerMultiplier`, `DamageMultiplier`, `DamageVariance`, `ArmorMultiplier`, `ExperienceMultiplier`, `MinLevelHealth`, `MaxLevelHealth`, `MinLevelMana`, `MaxLevelMana`, `MinMeleeDmg`, `MaxMeleeDmg`, `MinRangedDmg`, `MaxRangedDmg`, `Armor`, `MeleeAttackPower`, `RangedAttackPower`, `MeleeBaseAttackTime`, `RangedBaseAttackTime`, `DamageSchool`, `MinLootGold`, `MaxLootGold`, `LootId`, `PickpocketLootId`, `SkinningLootId`, `KillCredit1`, `KillCredit2`, `MechanicImmuneMask`, `SchoolImmuneMask`, `ResistanceHoly`, `ResistanceFire`, `ResistanceNature`, `ResistanceFrost`, `ResistanceShadow`, `ResistanceArcane`, `PetSpellDataId`, `MovementType`, `TrainerType`, `TrainerSpell`, `TrainerClass`, `TrainerRace`, `TrainerTemplateId`, `VendorTemplateId`, `EquipmentTemplateId`, `GossipMenuId`, `visibilityDistanceType`, `AIName`, `ScriptName`) VALUES (995001, 'Dungeon 1', 'Event', NULL, 70, 70, 0, 22387, 22387, 22387, 22387, 35, 1, 0, 7, 3, 3, 0, 4224, 33024, 0, 0, 0, 1, 1.14286, 20, 0, 0, 0, 0, 1, 3, 0, 1, 1, 1, 1, 1, 1, 100, 100, 100, 100, 7, 7, 1.76, 2.42, 15, 3, 100, 2000, 2200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '');
REPLACE INTO `world-tbc`.`creature_template`(`Entry`, `Name`, `SubName`, `IconName`, `MinLevel`, `MaxLevel`, `HeroicEntry`, `ModelId1`, `ModelId2`, `ModelId3`, `ModelId4`, `Faction`, `Scale`, `Family`, `CreatureType`, `InhabitType`, `RegenerateStats`, `RacialLeader`, `NpcFlags`, `UnitFlags`, `DynamicFlags`, `ExtraFlags`, `CreatureTypeFlags`, `SpeedWalk`, `SpeedRun`, `Detection`, `CallForHelp`, `Pursuit`, `Leash`, `Timeout`, `UnitClass`, `Rank`, `Expansion`, `HealthMultiplier`, `PowerMultiplier`, `DamageMultiplier`, `DamageVariance`, `ArmorMultiplier`, `ExperienceMultiplier`, `MinLevelHealth`, `MaxLevelHealth`, `MinLevelMana`, `MaxLevelMana`, `MinMeleeDmg`, `MaxMeleeDmg`, `MinRangedDmg`, `MaxRangedDmg`, `Armor`, `MeleeAttackPower`, `RangedAttackPower`, `MeleeBaseAttackTime`, `RangedBaseAttackTime`, `DamageSchool`, `MinLootGold`, `MaxLootGold`, `LootId`, `PickpocketLootId`, `SkinningLootId`, `KillCredit1`, `KillCredit2`, `MechanicImmuneMask`, `SchoolImmuneMask`, `ResistanceHoly`, `ResistanceFire`, `ResistanceNature`, `ResistanceFrost`, `ResistanceShadow`, `ResistanceArcane`, `PetSpellDataId`, `MovementType`, `TrainerType`, `TrainerSpell`, `TrainerClass`, `TrainerRace`, `TrainerTemplateId`, `VendorTemplateId`, `EquipmentTemplateId`, `GossipMenuId`, `visibilityDistanceType`, `AIName`, `ScriptName`) VALUES (995002, 'Dungeon 2', 'Event', NULL, 70, 70, 0, 22387, 22387, 22387, 22387, 35, 1, 0, 7, 3, 3, 0, 4224, 33024, 0, 0, 0, 1, 1.14286, 20, 0, 0, 0, 0, 1, 3, 0, 1, 1, 1, 1, 1, 1, 100, 100, 100, 100, 7, 7, 1.76, 2.42, 15, 3, 100, 2000, 2200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '');
REPLACE INTO `world-tbc`.`creature_template`(`Entry`, `Name`, `SubName`, `IconName`, `MinLevel`, `MaxLevel`, `HeroicEntry`, `ModelId1`, `ModelId2`, `ModelId3`, `ModelId4`, `Faction`, `Scale`, `Family`, `CreatureType`, `InhabitType`, `RegenerateStats`, `RacialLeader`, `NpcFlags`, `UnitFlags`, `DynamicFlags`, `ExtraFlags`, `CreatureTypeFlags`, `SpeedWalk`, `SpeedRun`, `Detection`, `CallForHelp`, `Pursuit`, `Leash`, `Timeout`, `UnitClass`, `Rank`, `Expansion`, `HealthMultiplier`, `PowerMultiplier`, `DamageMultiplier`, `DamageVariance`, `ArmorMultiplier`, `ExperienceMultiplier`, `MinLevelHealth`, `MaxLevelHealth`, `MinLevelMana`, `MaxLevelMana`, `MinMeleeDmg`, `MaxMeleeDmg`, `MinRangedDmg`, `MaxRangedDmg`, `Armor`, `MeleeAttackPower`, `RangedAttackPower`, `MeleeBaseAttackTime`, `RangedBaseAttackTime`, `DamageSchool`, `MinLootGold`, `MaxLootGold`, `LootId`, `PickpocketLootId`, `SkinningLootId`, `KillCredit1`, `KillCredit2`, `MechanicImmuneMask`, `SchoolImmuneMask`, `ResistanceHoly`, `ResistanceFire`, `ResistanceNature`, `ResistanceFrost`, `ResistanceShadow`, `ResistanceArcane`, `PetSpellDataId`, `MovementType`, `TrainerType`, `TrainerSpell`, `TrainerClass`, `TrainerRace`, `TrainerTemplateId`, `VendorTemplateId`, `EquipmentTemplateId`, `GossipMenuId`, `visibilityDistanceType`, `AIName`, `ScriptName`) VALUES (995003, 'Dungeon 3', 'Event', NULL, 70, 70, 0, 22387, 22387, 22387, 22387, 35, 1, 0, 7, 3, 3, 0, 4224, 33024, 0, 0, 0, 1, 1.14286, 20, 0, 0, 0, 0, 1, 3, 0, 1, 1, 1, 1, 1, 1, 100, 100, 100, 100, 7, 7, 1.76, 2.42, 15, 3, 100, 2000, 2200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '');

/* 
item add to vendor dungeon 1
*/
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16695, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16690, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16697, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16692, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16696, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16694, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16691, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16698, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16701, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16700, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16703, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16705, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16702, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16699, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16704, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16667, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16669, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16666, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16671, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16672, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16673, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16668, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16670, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16727, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16729, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16726, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16722, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16724, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16723, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16728, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16725, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16686, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16689, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16688, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16683, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16684, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16685, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16687, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16682, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16707, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16708, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16721, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16710, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16712, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16713, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16709, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16711, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16731, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16733, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16730, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16735, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16737, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16736, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16732, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16734, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16720, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16718, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16706, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16714, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16717, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16716, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16719, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995001, 16715, 0, 0, 0, 0, '');


/* 
item add to vendor dungeon 2
*/

REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 21999, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22001, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 21997, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 21996, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 21998, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 21994, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22000, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 21995, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22013, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22016, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22060, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22011, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22015, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22010, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22017, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22061, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22005, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22008, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22009, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22004, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22006, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22002, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22007, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22003, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22074, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22073, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22075, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22071, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22077, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22070, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22072, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22076, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22109, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22112, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22113, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22108, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22110, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22106, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22111, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22107, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22065, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22068, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22069, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22063, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22066, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22062, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22067, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22064, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22091, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22093, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22089, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22088, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22090, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22086, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22092, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22087, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22097, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22101, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22102, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22095, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22099, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22098, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22100, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22096, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22080, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22082, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22083, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22079, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22081, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22078, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22085, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995002, 22084, 0, 0, 0, 0, '');


/* 
item add to vendor dungeon 3
*/
	
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28414, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27776, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28204, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27509, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27908, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28275, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27801, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28228, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27474, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27874, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28350, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27803, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28205, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27475, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27977, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28192, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27713, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28401, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27528, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27936, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28225, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27771, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28403, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27497, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27870, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28413, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27775, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28230, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27536, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27875, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28278, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27738, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28229, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27508, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27838, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28193, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27796, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28191, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27465, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27907, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28348, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27737, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28202, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27468, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27873, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28415, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27778, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28232, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27537, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27948, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28285, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27739, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28203, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27535, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27839, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28349, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27802, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28231, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27510, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27909, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28224, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27797, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 28264, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27531, 0, 0, 0, 0, '');
REPLACE INTO `world-tbc`.`npc_vendor`(`entry`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `condition_id`, `comments`) VALUES (995003, 27837, 0, 0, 0, 0, '');